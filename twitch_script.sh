#!/bin/bash
dir=$(dirname -- "$0")
clientid=""

function add_names {
   > $dir/ids
    echo $1 >> $dir/names.txt

  while read line; do
    curl -s -H "Client-ID: $clientid" -X GET https://api.twitch.tv/helix/users?login=$line \
    | jq -r '.data[0].id' >> $dir/ids
  done < "$dir/names.txt"
}

function get_status {
  while read line; do
    status=$(curl -s -H "Client-ID: $clientid" -X GET https://api.twitch.tv/helix/streams?user_id=$line \
      | jq -r '.data[0]')
    if [ "$status" != "null" ]; then
      name=$(echo $status | jq -r '.user_name')
      onlines+=($name)
    fi
  done < "$dir/ids"

  for i in "${onlines[@]}"
  do
    print+="$i\n"
  done
  choice=$(echo -e $print | dmenu -l 5 -i)
  mpv https://www.twitch.tv/$choice
}

if [ $# -eq 0 ] ; then
  get_status
fi
if [ "$1" == "-a" ] ; then
  add_names $2
fi

