# Bash script that launches stream to mpv

## Shows list of predefined streams which are online in dmenu. When stream is selected, launches mpv and starts playing stream. 


## Dependiencies
  - twitch api credentials
  - dmenu
  - mpv

###
  - Currently using with i3 and have keybind to use script.


### How to
- add your Twitch client-id to file
- argument -a [name] adds streams name to names.txt and generates its id

